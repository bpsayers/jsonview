define ['factory!cs!jsonview', 
		'jquery'], 
		
		(jsonviewDefinition, $) ->
			describe 'jsonview', () ->
				json = null
				jsonview = null
				$output = null

				$find = (selector) -> $output.find(".jsonview #{selector}")

				expectPropertyToBeRendered = (containerSelector, name, value) ->
					describe 'when rendering', () ->
						beforeEach () -> jsonview.render($output, json)

						it 'should render the property name', () ->
							expect($find(containerSelector).find('.key')).toHaveText(name)

						it 'should render the property value', () ->
							expect($find(containerSelector).find('.value')).toHaveText(value)

				beforeEach () ->
					jsonview = jsonviewDefinition($)
					$output = $('<div></div>')

				it 'should do nothing when given no container', () ->
					jsonview.render(null, {})

					expect($find()).not.toExist()

				it 'should render json as the first title', () ->
					jsonview.render($output)

					expect($find('.pair .key')).toHaveText('json')

				it 'should render an undefined json object when given no json', () ->
					jsonview.render($output)

					expect($find('.pair .value')).toHaveText('undefined')

				it 'should render an empty json object when given an empty json object', () ->
					jsonview.render($output, {})

					expect($find('.pair .value')).toBeEmpty()

				describe 'when given a json object with one string property', () ->
					beforeEach () -> json = { property1: 'value1' }

					expectPropertyToBeRendered('.pair .value .pair', 'property1', '"value1"')

				describe 'when given a json object with one date property', () ->
					beforeEach () -> json = { property1: new Date(2012, 4, 16, 21, 45, 30, 500) }

					expectPropertyToBeRendered('.pair .value .pair', 'property1', '16/5/2012 21:45:30.500')

				describe 'when given a json object with one number property', () ->
					beforeEach () -> json = { property1: 10 }

					expectPropertyToBeRendered('.pair .value .pair', 'property1', '10')

				describe 'when given a json object with one function property', () ->
					beforeEach () -> json = {property1: () ->}

					expectPropertyToBeRendered('.pair .value .pair', 'property1', 'function() { ... }')

				describe 'when given a json object with two string properties', () ->
					beforeEach () -> json = {property1: 'value1', property2: 'value2'}

					expectPropertyToBeRendered('.pair .value .pair:eq(0)', 'property1', '"value1"')
					expectPropertyToBeRendered('.pair .value .pair:eq(1)', 'property2', '"value2"')

				describe 'when given a json object with one nested json object property', () ->
					beforeEach () -> json = {property1: {innerProperty1: 'innerValue1'}}

					expectPropertyToBeRendered('.pair .value .pair .value .pair', 'innerProperty1', '"innerValue1"')

				describe 'when given a json object with one array property', () ->
					beforeEach () -> json = {property1: ['value1', 'value2']}

					expectPropertyToBeRendered('.pair .value .pair .value .pair:eq(0)', '[0]', '"value1"')
					expectPropertyToBeRendered('.pair .value .pair .value .pair:eq(1)', '[1]', '"value2"')




