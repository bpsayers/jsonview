require.config({
  baseUrl: 'Scripts/src',
  paths: {
    CoffeeScript: '../libs/coffeescript/CoffeeScript-1.3.3',
    cs: '../libs/require/plugins/cs-0.4.0',
    jquery: '../libs/jquery/jquery-1.7.2',
    text: '../libs/require/plugins/text-1.0.8',
    underscore: '../libs/underscore/1.3.2'
  }
});