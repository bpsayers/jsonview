define ['jquery'], 

	($) ->

		formatArray = (arr) ->
			markup = ''
			markup += formatPair("[#{i}]", value) for value, i in arr
			markup

		formatDate = (value) ->
			date = "#{value.getDate()}/#{value.getMonth() + 1}/#{value.getFullYear()}"
			time = "#{value.getHours()}:#{value.getMinutes()}:#{value.getSeconds()}.#{value.getMilliseconds()}"
			"#{date} #{time}"

		formatFunction = () -> 'function() { ... }'

		formatNumber = (value) -> value

		formatObject = (json) ->
			markup = ''
			markup += formatPair(key, value) for own key, value of json 
			markup

		formatPair = (key, value) -> 
			"""
			<div class="pair">
				<div class="key">#{key}</div>
				#{parseJson(value)}
			</div>
			"""

		formatString = (value) -> "\"#{value}\""

		formatUndefined = () -> 'undefined'

		formatValue = (value) -> """<div class="value">#{value}</div>"""

		isArray = (value) -> value instanceof Array			

		isDate = (value) -> value instanceof Date

		isFunction = (value) -> typeof value is 'function'

		isNumber = (value) -> typeof value is 'number'

		isObject = (value) -> typeof value is 'object'

		isString = (value) -> typeof value is 'string'

		isUndefined = (value) -> not value

		typeChecks = [
			{isType: isString, formatType: formatString},
			{isType: isNumber, formatType: formatNumber},
			{isType: isFunction, formatType: formatFunction},
			{isType: isDate, formatType: formatDate},
			{isType: isUndefined, formatType: formatUndefined},
			{isType: isArray, formatType: formatArray},
			{isType: isObject, formatType: formatObject}
		]

		parseJson = (json) ->
			for type in typeChecks
				return formatValue(type.formatType(json)) if type.isType(json)

		methods = {}

		methods.render = ($container, json) ->
			$('<div class="jsonview"></div>').html(parseJson({json: json})).appendTo($container) if $container

		return methods