var requirejs = require('./libs/node/node_modules/requirejs');

requirejs.optimize({
    baseUrl: '../Scripts/src',
    optimize: 'none',
    paths: {
        CoffeeScript: '../libs/coffeescript/CoffeeScript-1.3.3',
        cs: '../libs/require/plugins/csBuild-0.4.0',
        csBuild: '../libs/require/plugins/cs-0.4.0',
        jquery: '../libs/jquery/jquery-1.7.2'
    },
    name: 'cs!jsonview',
    out: '../Scripts/jsonview.js',
    exclude: ['CoffeeScript']
});